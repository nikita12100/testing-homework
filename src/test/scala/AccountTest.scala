import org.scalatest.Inspectors
import org.scalatest.flatspec._
import org.scalatest.matchers.should.Matchers

class AccountTest extends AnyFlatSpec with Matchers with Inspectors {
  val account: Account = Account(100)


  it should "add amount to account" in {
    account.add(500).balance shouldBe 600
  }

  it should "subtract amount to account" in {
    account.add(500).subtract(100).balance shouldBe 500
  }

  it should "decline operations with negative result" in {
    account.subtract(100).subtract(100).balance shouldBe 0
  }

}
