import org.scalatest.Inspectors
import org.scalatest.flatspec._
import org.scalatest.matchers.should.Matchers

class RangeTest extends AnyFlatSpec with Matchers with Inspectors {

  it should "return interval l < r" in {
    Range(0, 1) shouldBe RangeInt(0, 1)
  }

  it should "return empty interval for l > r" in {
    Range(2, 1) shouldBe NilRange
  }

  it should "return [2, 3] for intersection [1, 4] with [2, 3]" in {
    Range(1, 4).intersect(Range(2, 3)) shouldBe Range(2, 3)
  }

  it should "return empty range for intersection [1, 2] with [3, 4]" in {
    Range(1, 2).intersect(Range(3, 4)) shouldBe NilRange
  }

  it should "return [2, 3] for intersection [2, 3] with [1, 4]" in {
    Range(2, 3).intersect(Range(1, 4)) shouldBe Range(2, 3)
  }

  it should "return [2, 2] for intersection [1, 2] with [2, 3]" in {
    Range(1, 2).intersect(Range(2, 3)) shouldBe Range(2, 2)
  }

  it should "return [1, 3] for union [1, 2] with [2, 3]" in {
    Range(1, 2).union(Range(2, 3)) shouldBe Range(1, 3)
  }

  it should "return empty range for union [1, 2] with [3, 4]" in {
    Range(1, 2).union(Range(3, 4)) shouldBe NilRange
  }

  it should "return [1, 4] for union [2, 3] with [1, 4]" in {
    Range(2, 3).union(Range(1, 4)) shouldBe Range(1, 4)
  }

  it should "return true for empty range" in {
    NilRange.isEmpty shouldBe true
  }

  it should "return false for [1, 2]" in {
    Range(1, 2).isEmpty shouldBe false
  }

  it should "return true for inner point" in {
    Range(1, 3).contains(2) shouldBe true
  }

  it should "return false for outer point" in {
    Range(1, 3).contains(5) shouldBe false
  }

  it should "return false for empty range" in {
    NilRange.contains(0) shouldBe false
  }

  it should "return check \"equals\" method" in {
    NilRange == NilRange shouldBe true
    Range(1, 2) == Range(1, 2) shouldBe true
    Range(-1, 0) == NilRange shouldBe false
    Range(5, 5) == Range(6, 7) shouldBe false
  }

  it should "check \"isIntersectedWith\" method" in {
    Range(1, 2).isIntersectedWith(Range(2, 3)) shouldBe true
    Range(5, 8).isIntersectedWith(Range(9, 10)) shouldBe false
    Range(-5, 0).isIntersectedWith(NilRange) shouldBe false
    NilRange.isIntersectedWith(NilRange) shouldBe false
  }

  it should "check \"isSubsetOf\" method" in {
    NilRange.isSubsetOf(NilRange) shouldBe true
    Range(2, 3).isSubsetOf(Range(1, 5)) shouldBe true
    NilRange.isSubsetOf(Range(-1, 0)) shouldBe true
    Range(5, 8).isSubsetOf(Range(10, 17)) shouldBe false
    Range(4, 10).isSubsetOf(NilRange) shouldBe false
  }

  it should "return min for [3, 4] union [0, 5]" in {
    Range(3, 4).union(Range(0, 5)).min shouldBe Some(0)
  }
}
