import java.time.LocalDateTime


case class Amount(amount: Int) {
  override def toString: String =
    if (amount >= 0) s"+$amount"
    else s"$amount"
}

sealed trait Status

object Status {
  case object Normal extends Status

  case object Declined extends Status
}

case class Account(balance: Int = 0, transactions: Seq[(LocalDateTime, Amount, Int, Status)] = List()) {
  def add(amount: Int): Account = amount match {
    case x if x + balance < 0 => copy(
      transactions = transactions :+ (LocalDateTime.now(), Amount(amount), balance, Status.Declined)
    )
    case _ => copy(
      balance + amount,
      transactions :+ (LocalDateTime.now(), Amount(amount), balance + amount, Status.Normal)
    )
  }

  def subtract(amount: Int): Account = add(-amount)

}
