sealed trait Range {
  def min: Option[Int] = this match {
    case RangeInt(start, _) => Some(start)
    case _ => None
  }

  def max: Option[Int] = this match {
    case RangeInt(_, end) => Some(end)
    case _ => None
  }

  def intersect(that: Range): Range = {
    (this, that) match {
      case (RangeInt(min1, max1), RangeInt(min2, max2)) =>
        if ((max1 - min2) * (max2 - min1) < 0) NilRange
        else {
          val min = if (min1 < min2) min2 else min1
          val max = if (max1 < max2) max1 else max2
          Range(min, max)
        }
      case _ => NilRange
    }
  }

  def union(that: Range): Range = {
    (this, that) match {
      case (RangeInt(min1, max1), RangeInt(min2, max2)) =>
        if ((max1 - min2) * (max2 - min1) < 0) NilRange
        else {
          val min = if (min1 < min2) min1 else min2
          val max = if (max1 < max2) max2 else max1
          Range(min, max)
        }

      case _ => NilRange
    }
  }


  def contains(point: Int): Boolean = this match {
    case RangeInt(start, end) if point >= start & point <= end => true
    case _ => false
  }


  def isEmpty: Boolean = this match {
    case NilRange => true
    case _ => false
  }

  def isIntersectedWith(that: Range): Boolean = (this, that) match {
    case (RangeInt(min1, max1), RangeInt(min2, max2))
      if (max1 - min2) * (max2 - min1) >= 0 => true
    case _ => false
  }

  def isSubsetOf(that: Range): Boolean = (this, that) match {
    case (NilRange, _) => true
    case (RangeInt(minIn, maxIn), RangeInt(minOut, maxOut))
      if minIn >= minOut & maxIn <= maxOut => true
    case _ => false
  }
}

object Range {
  def apply(start: Int, end: Int): Range = end - start match {
    case x if x >= 0 => RangeInt(start, end)
    case _ => NilRange
  }
}

case object NilRange extends Range

case class RangeInt(start: Int, end: Int) extends Range


